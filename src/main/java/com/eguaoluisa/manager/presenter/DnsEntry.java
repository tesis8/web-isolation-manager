package com.eguaoluisa.manager.presenter;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DnsEntry {
    private String ip;
    private String url;
}
