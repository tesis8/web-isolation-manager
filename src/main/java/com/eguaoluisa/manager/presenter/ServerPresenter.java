package com.eguaoluisa.manager.presenter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ServerPresenter {
    private String id;
    private String name;
    private String ip;
}
