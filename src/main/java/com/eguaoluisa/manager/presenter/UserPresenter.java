package com.eguaoluisa.manager.presenter;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString(of = "id")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserPresenter {

    private Integer id;
    private String username;
    private String name;
    private String lastName;
    private String encodedPassword;
    @Builder.Default
    private List<String> permissions = new ArrayList<>();
}
