package com.eguaoluisa.manager.presenter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginRequestPresenter {
    private String email;
    private String password;
    private boolean returnSecureToken;
}
