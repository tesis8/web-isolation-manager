package com.eguaoluisa.manager.client;

import com.eguaoluisa.manager.presenter.FirebaseResponse;
import com.eguaoluisa.manager.presenter.LoginRequestPresenter;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "firebase-auth", url = "https://identitytoolkit.googleapis.com/v1/")
public interface FirebaseAuthClient {
    @PostMapping("accounts:signInWithPassword")
    FirebaseResponse login(@RequestParam("key") String key, @RequestBody LoginRequestPresenter loginRequestPresenter);
}
