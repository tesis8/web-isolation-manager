package com.eguaoluisa.manager.client;

import com.eguaoluisa.manager.presenter.ServerPresenter;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(name = "firebase-database", url = "https://web-isolation-manager-default-rtdb.firebaseio.com/")
public interface FirebaseDatabaseClient {
    @GetMapping("servers.json")
    Map<String, ServerPresenter> findAll();

    @PostMapping("servers.json")
    void save(@RequestBody ServerPresenter serverPresenter);

    @PutMapping("servers/{id}.json")
    void modify(@PathVariable("id") String id, @RequestBody ServerPresenter serverPresenter);

    @DeleteMapping("servers/{id}.json")
    void delete(@PathVariable("id") String id);
}
