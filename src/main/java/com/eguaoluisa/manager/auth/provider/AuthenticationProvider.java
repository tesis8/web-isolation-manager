package com.eguaoluisa.manager.auth.provider;

import com.eguaoluisa.manager.presenter.FirebaseResponse;
import com.eguaoluisa.manager.presenter.UserPresenter;
import com.eguaoluisa.manager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AuthenticationProvider implements org.springframework.security.authentication.AuthenticationProvider {

    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        FirebaseResponse firebaseResponse = userService.loginWithFirebase(authentication.getPrincipal().toString(), authentication.getCredentials().toString());
        if (firebaseResponse == null) {
            throw new BadCredentialsException("El nombre de usuario o contraseña es incorrecto");
        }
        UserPresenter userPresenter = UserPresenter.builder().username(firebaseResponse.getEmail()).build();
        if (firebaseResponse.getIdToken() != null) {
            userPresenter.getPermissions().add("is-administrator");
            Set<SimpleGrantedAuthority> simpleGrantedAuthorities = userPresenter.getPermissions().stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toSet());
            return new UsernamePasswordAuthenticationToken(userPresenter, null, simpleGrantedAuthorities);
        }
        throw new BadCredentialsException("El nombre de usuario o contraseña es incorrecto");
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.equals(authentication);
    }

}
