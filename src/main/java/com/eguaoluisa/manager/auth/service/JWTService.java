package com.eguaoluisa.manager.auth.service;

import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Map;

public interface JWTService {

    String HEADER_STRING = "Authorization";

    String create(Authentication authentication);

    Collection<SimpleGrantedAuthority> getRoles(DecodedJWT decodedJWT);

    DecodedJWT decode(String token);

    Map<String, Claim> getClaims(String token);

}
