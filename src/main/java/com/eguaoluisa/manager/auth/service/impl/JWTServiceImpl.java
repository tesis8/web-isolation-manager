package com.eguaoluisa.manager.auth.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.eguaoluisa.manager.auth.service.JWTService;
import com.eguaoluisa.manager.presenter.UserPresenter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class JWTServiceImpl implements JWTService {

    @Value("${app.config.security.token.expirationDate}")
    private long expirationDate;
    @Value("${app.config.security.token.secret}")
    private String tokenSecret;
    private String encodeSecret;

    @PostConstruct
    public void configure() {
        this.encodeSecret = Base64Utils.encodeToString(tokenSecret.getBytes());
    }

    @Override
    public String create(Authentication authentication) {
        UserPresenter user = (UserPresenter) authentication.getPrincipal();
        Map<String, Object> headerClaims = new HashMap<>();
        headerClaims.put("roles", authentication.getAuthorities());
        return JWT.create()
                .withHeader(headerClaims)
                .withClaim("user", user.getLastName() != null ? user.getName() + " " + user.getLastName() : user.getName())
                .withSubject(user.getUsername())
                .withIssuedAt(new Date())
                .withExpiresAt(new Date(System.currentTimeMillis() + expirationDate))
                .sign(Algorithm.HMAC256(encodeSecret));
    }

    @Override
    public Map<String, Claim> getClaims(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(encodeSecret);
            com.auth0.jwt.JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT jwt = verifier.verify(token);
            return jwt.getClaims();
        } catch (Exception exception) {
            return null;
        }
    }

    @Override
    public Collection<SimpleGrantedAuthority> getRoles(DecodedJWT decodedJWT) {
        Claim claimRoles = decodedJWT.getHeaderClaim("roles");
        Set<Object> headerClaims = claimRoles.as(Set.class);
        Collection<SimpleGrantedAuthority> result = new HashSet<>();
        headerClaims.forEach(claim -> {
            Map<String, Object> roleMap = (Map<String, Object>) claim;
            roleMap.entrySet().forEach(stringObjectEntry -> {
                SimpleGrantedAuthority simpleGrantedAuthority =
                        new SimpleGrantedAuthority(stringObjectEntry.getValue().toString());
                result.add(simpleGrantedAuthority);
            });
        });
        return result;
    }

    @Override
    public DecodedJWT decode(String token) {
        Algorithm algorithm = Algorithm.HMAC256(this.encodeSecret);
        JWTVerifier verifier = JWT.require(algorithm).build();
        return verifier.verify(token);
    }

}
