package com.eguaoluisa.manager.auth.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.eguaoluisa.manager.auth.service.JWTService;
import com.eguaoluisa.manager.presenter.UserPresenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Component
public class UserInterceptor implements HandlerInterceptor {

    @Autowired
    private UserEnvironment userEnvironment;

    @Autowired
    private JWTService jwtService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        userEnvironment.setUserPresenter(new UserPresenter());
        userEnvironment.setToken(request.getHeader("Authorization"));
        if (userEnvironment.getToken() != null) {
            Map<String, Claim> claims = JWT.decode(userEnvironment.getToken()).getClaims();
            if (claims != null) {
                userEnvironment.getUserPresenter().setUsername(claims.get("sub").asString());
            }
        }
        return true;
    }

}
