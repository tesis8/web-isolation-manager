package com.eguaoluisa.manager.auth.config;

import com.eguaoluisa.manager.presenter.UserPresenter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEnvironment {
    private UserPresenter userPresenter;
    private String token;
}
