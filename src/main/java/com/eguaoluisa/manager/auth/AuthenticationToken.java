package com.eguaoluisa.manager.auth;

import org.springframework.security.authentication.AbstractAuthenticationToken;

import java.util.Collections;

public class AuthenticationToken extends AbstractAuthenticationToken {

    private String username;
    private String password;

    public AuthenticationToken(String username, String password) {
        super(Collections.emptyList());
        this.username = username;
        this.password = password;
    }

    @Override
    public Object getCredentials() {
        return password;
    }

    @Override
    public Object getPrincipal() {
        return username;
    }
}
