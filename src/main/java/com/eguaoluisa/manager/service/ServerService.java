package com.eguaoluisa.manager.service;

import com.eguaoluisa.manager.presenter.ServerPresenter;

import java.util.List;

public interface ServerService {
    List<ServerPresenter> findAll();

    void saveService(ServerPresenter serverPresenter);

    void modifyService(ServerPresenter serverPresenter);

    void deleteService(String id);
}
