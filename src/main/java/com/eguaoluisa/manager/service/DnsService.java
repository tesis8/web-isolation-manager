package com.eguaoluisa.manager.service;

import com.eguaoluisa.manager.presenter.DnsEntry;

import java.io.IOException;
import java.util.List;

public interface DnsService {
    List<DnsEntry> findAll() throws IOException;

    void save(DnsEntry dnsEntry) throws IOException;

    void update(DnsEntry dnsEntry) throws IOException;

    void delete(String url, String ip) throws IOException;

    void deleteAll() throws IOException;
}
