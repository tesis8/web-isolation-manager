package com.eguaoluisa.manager.service.impl;

import com.eguaoluisa.manager.presenter.DnsEntry;
import com.eguaoluisa.manager.repository.DnsRepository;
import com.eguaoluisa.manager.service.DnsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@Service
public class DnsServiceImpl implements DnsService {

    final String PATTERN = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";


    @Autowired
    private DnsRepository dnsRepository;


    @Override
    public List<DnsEntry> findAll() throws IOException {
        return dnsRepository.getAll();
    }

    @Override
    public void save(DnsEntry dnsEntry) throws IOException {
        if (dnsEntry.getUrl().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "El dominio se encuentra vacío");
        }
        if (!dnsEntry.getIp().matches(PATTERN)) {
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "La IP no es correcta");
        }
        dnsRepository.getAll().forEach(dnsEntry1 -> {
            if (dnsEntry1.getUrl().equals(dnsEntry.getUrl())) {
                throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "El dominio " + dnsEntry.getUrl() + " ya se encuentra registrado");
            }
        });
        this.sanitizeURL(dnsEntry);
        dnsRepository.save(dnsEntry);
    }

    private void sanitizeURL(DnsEntry dnsEntry) {
        String finalUrl = dnsEntry.getUrl().replace("https://", "");
        finalUrl = finalUrl.replace("http://", "");
        finalUrl = finalUrl.replace(":", "");
        finalUrl = finalUrl.split("\\?")[0];
        finalUrl = finalUrl.replace("/", "");
        dnsEntry.setUrl(finalUrl);
    }

    @Override
    public void update(DnsEntry dnsEntry) throws IOException {
        if (dnsEntry.getUrl().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "El dominio se encuentra vacío");
        }
        if (!dnsEntry.getIp().matches(PATTERN)) {
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "La IP no es correcta");
        }
        dnsRepository.updateIp(dnsEntry);
    }

    @Override
    public void delete(String url, String ip) throws IOException {
        DnsEntry dnsEntry = DnsEntry.builder().url(url).ip(ip).build();
        dnsRepository.delete(dnsEntry);
    }

    @Override
    public void deleteAll() throws IOException {
        for (DnsEntry dnsEntry : dnsRepository.getAll()) {
            dnsRepository.delete(dnsEntry);
        }
    }
}
