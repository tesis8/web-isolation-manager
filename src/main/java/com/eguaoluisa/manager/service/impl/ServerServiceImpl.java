package com.eguaoluisa.manager.service.impl;

import com.eguaoluisa.manager.client.FirebaseDatabaseClient;
import com.eguaoluisa.manager.presenter.ServerPresenter;
import com.eguaoluisa.manager.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ServerServiceImpl implements ServerService {

    @Autowired
    private FirebaseDatabaseClient firebaseDatabaseClient;

    @Override
    public List<ServerPresenter> findAll() {
        Map<String, ServerPresenter> all = firebaseDatabaseClient.findAll();
        List<ServerPresenter> servers = new ArrayList<>();
        if(all != null) {
            all.keySet().forEach(s -> {
                ServerPresenter serverPresenter = all.get(s);
                serverPresenter.setId(s);
                servers.add(serverPresenter);
            });
        }
        return servers;

    }

    @Override
    public void saveService(ServerPresenter serverPresenter) {
        firebaseDatabaseClient.save(serverPresenter);
    }

    @Override
    public void modifyService(ServerPresenter serverPresenter) {
        String id = serverPresenter.getId();
        serverPresenter.setId(null);
        firebaseDatabaseClient.modify(id, serverPresenter);
    }

    @Override
    public void deleteService(String id) {
        firebaseDatabaseClient.delete(id);
    }
}
