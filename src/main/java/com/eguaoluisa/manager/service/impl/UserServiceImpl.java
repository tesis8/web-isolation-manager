package com.eguaoluisa.manager.service.impl;

import com.eguaoluisa.manager.client.FirebaseAuthClient;
import com.eguaoluisa.manager.presenter.FirebaseResponse;
import com.eguaoluisa.manager.presenter.LoginRequestPresenter;
import com.eguaoluisa.manager.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private FirebaseAuthClient firebaseAuthClient;

    @Value("${app.config.firebase-token}")
    private String firebaseToken;

    @Override
    public FirebaseResponse loginWithFirebase(String email, String password) {
        return firebaseAuthClient.login(firebaseToken, LoginRequestPresenter.builder().email(email).password(password).returnSecureToken(true).build());
    }
}
