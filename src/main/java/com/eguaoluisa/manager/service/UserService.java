package com.eguaoluisa.manager.service;

import com.eguaoluisa.manager.presenter.FirebaseResponse;
import com.eguaoluisa.manager.presenter.UserPresenter;

public interface UserService {
    FirebaseResponse loginWithFirebase(String email, String password);
}
