package com.eguaoluisa.manager.filter;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.eguaoluisa.manager.auth.service.JWTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 2)
public class JWTAuthorizationFilter extends OncePerRequestFilter {

    @Autowired
    private JWTService jwtService;

    private static final String FILTER_APPLIED = "__spring_security_scpf_applied";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String jwtToken = request.getHeader(JWTService.HEADER_STRING);
        if (jwtToken != null && !jwtToken.isEmpty()) {
            DecodedJWT decodedJWT;
            decodedJWT = jwtService.decode(jwtToken);
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(decodedJWT.getSubject(),
                    jwtToken, jwtService.getRoles(decodedJWT));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            request.setAttribute(FILTER_APPLIED, true);
        }
        filterChain.doFilter(request, response);
    }


}
