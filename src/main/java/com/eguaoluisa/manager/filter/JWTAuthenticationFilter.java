package com.eguaoluisa.manager.filter;

import com.eguaoluisa.manager.auth.service.JWTService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 1)
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    @Autowired
    private JWTService jwtService;

    private static final String FILTER_APPLIED = "__spring_security_scpf_applied";

    public JWTAuthenticationFilter(ProviderManager providerManager) {
        this.setAuthenticationManager(providerManager);
        this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login", "POST"));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try {
            String content = IOUtils.toString(request.getReader());
            LoginRequest loginRequest = new ObjectMapper().readValue(content, LoginRequest.class);
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword());
            this.setDetails(request, usernamePasswordAuthenticationToken);
            request.setAttribute(FILTER_APPLIED, true);
            return this.getAuthenticationManager().authenticate(usernamePasswordAuthenticationToken);
        } catch (IOException e) {
            throw new IllegalArgumentException("RequestBody could not be read");
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        String jwtToken = jwtService.create(authResult);
        response.addHeader(JWTService.HEADER_STRING, jwtToken);
        response.addHeader("Access-Control-Expose-Headers", "Authorization");
        response.setStatus(HttpStatus.OK.value());

        Map<String, Object> body = new HashMap<>();
        body.put("token", jwtToken);

        response.getWriter().write(new ObjectMapper().writeValueAsString(body));
        response.setContentType("application/json");
    }

    @NoArgsConstructor
    @Getter
    @Setter
    static class LoginRequest {
        private String username;
        private String password;
    }

}
