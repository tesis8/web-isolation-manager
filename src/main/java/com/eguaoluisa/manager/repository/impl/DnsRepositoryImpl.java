package com.eguaoluisa.manager.repository.impl;

import com.eguaoluisa.manager.presenter.DnsEntry;
import com.eguaoluisa.manager.repository.DnsRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class DnsRepositoryImpl implements DnsRepository {

    @Value("${app.config.path}")
    private String path;
    @Value("${app.config.pattern}")
    private String pattern;
    @Value("${app.config.separator}")
    private String separator;
    @Value("${app.config.temp}")
    private String tempPath;

    @Override
    public List<DnsEntry> getAll() throws IOException {
        List<DnsEntry> result = new ArrayList<>();
        Files.lines(Path.of(path)).forEach(line -> {
            String trimmed = line.trim();
            if (trimmed.startsWith(pattern)) {
                String filtered = trimmed.substring(pattern.length() + 1 + separator.length());
                String[] parts = filtered.split(separator);
                result.add(DnsEntry.builder().ip(parts[1]).url(parts[0]).build());
            }
        });
        return result;
    }

    @Override
    public void save(DnsEntry dnsEntry) throws IOException {
        Path filePath = Paths.get(path);
        String textToAppend = pattern + "=" + separator + dnsEntry.getUrl() + separator + dnsEntry.getIp() + System.lineSeparator();
        Files.write(filePath, textToAppend.getBytes(), StandardOpenOption.APPEND);
    }

    @Override
    public void delete(DnsEntry dnsEntry) throws IOException {
        cleanFile();
        Path tempFilePath = Paths.get(tempPath);
        List<String> lines = Files.lines(Path.of(path)).collect(Collectors.toList());
        for (String line : lines) {
            String trimmed = line.trim();
            if (trimmed.startsWith(pattern)) {
                String filtered = trimmed.substring(pattern.length() + 1 + separator.length());
                String[] parts = filtered.split(separator);
                if (!dnsEntry.getIp().equals(parts[1]) || !dnsEntry.getUrl().equals(parts[0])) {
                    Files.write(tempFilePath, (line + System.lineSeparator()).getBytes(), StandardOpenOption.APPEND);
                }
            } else {
                Files.write(tempFilePath, (line + System.lineSeparator()).getBytes(), StandardOpenOption.APPEND);
            }
        }
        FileCopyUtils.copy(new File(tempPath), new File(path));
        cleanFile();
    }

    @Override
    public void updateIp(DnsEntry dnsEntry) throws IOException {
        cleanFile();
        Path tempFilePath = Paths.get(tempPath);
        List<String> lines = Files.lines(Path.of(path)).collect(Collectors.toList());
        for (String line : lines) {
            String trimmed = line.trim();
            if (trimmed.startsWith(pattern)) {
                String filtered = trimmed.substring(pattern.length() + 1 + separator.length());
                String[] parts = filtered.split(separator);
                if (dnsEntry.getUrl().equals(parts[0])) {
                    String textToAppend = pattern + "=" + separator + dnsEntry.getUrl() + separator + dnsEntry.getIp() + System.lineSeparator();
                    Files.write(tempFilePath, textToAppend.getBytes(), StandardOpenOption.APPEND);
                } else {
                    Files.write(tempFilePath, (line + System.lineSeparator()).getBytes(), StandardOpenOption.APPEND);
                }
            } else {
                Files.write(tempFilePath, (line + System.lineSeparator()).getBytes(), StandardOpenOption.APPEND);
            }
        }
        FileCopyUtils.copy(new File(tempPath), new File(path));
        cleanFile();
    }

    private void cleanFile() throws IOException {
        PrintWriter writer = new PrintWriter(tempPath);
        writer.print("");
        writer.close();
    }
}
