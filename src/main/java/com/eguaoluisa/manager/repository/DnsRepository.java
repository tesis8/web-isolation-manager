package com.eguaoluisa.manager.repository;

import com.eguaoluisa.manager.presenter.DnsEntry;

import java.io.IOException;
import java.util.List;

public interface DnsRepository {
    List<DnsEntry> getAll() throws IOException;

    void save(DnsEntry dnsEntry) throws IOException;

    void delete(DnsEntry dnsEntry) throws IOException;

    void updateIp(DnsEntry dnsEntry) throws IOException;
}
