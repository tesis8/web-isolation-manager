package com.eguaoluisa.manager.controller;

import com.eguaoluisa.manager.presenter.ServerPresenter;
import com.eguaoluisa.manager.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ServerController {
    @Autowired
    private ServerService serverService;

    @GetMapping("/servers")
    public List<ServerPresenter> getAllServers() {
        return serverService.findAll();
    }

    @PostMapping("/server")
    public void save(@RequestBody ServerPresenter serverPresenter) {
        serverService.saveService(serverPresenter);
    }

    @PutMapping("/server")
    public void modify(@RequestBody ServerPresenter serverPresenter) {
        serverService.modifyService(serverPresenter);
    }

    @DeleteMapping("/server")
    public void delete(@RequestParam String id) {
        serverService.deleteService(id);
    }
}
