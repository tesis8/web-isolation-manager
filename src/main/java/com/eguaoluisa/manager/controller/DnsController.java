package com.eguaoluisa.manager.controller;

import com.eguaoluisa.manager.presenter.DnsEntry;
import com.eguaoluisa.manager.service.DnsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class DnsController {

    @Autowired
    private DnsService dnsService;

    @GetMapping("/entries")
    public List<DnsEntry> getEntries() throws IOException {
        return dnsService.findAll();
    }

    @PostMapping("/entry")
    public void saveEntry(@RequestBody DnsEntry dnsEntry) throws IOException {
        dnsService.save(dnsEntry);
    }

    @DeleteMapping("/entry")
    public void deleteEntry(@RequestParam String url, @RequestParam String ip) throws IOException {
        dnsService.delete(url, ip);
    }

    @PutMapping("/entry")
    public void updateEntry(@RequestBody DnsEntry dnsEntry) throws IOException {
        dnsService.update(dnsEntry);
    }

    @DeleteMapping("/entries")
    public void deleteEntries() throws IOException {
        dnsService.deleteAll();
    }
}
